#[macro_use]
extern crate log;
extern crate env_logger;
extern crate sha2;
extern crate num_cpus;


use std::collections::BTreeMap;
use std::env;
use std::fs;
use std::path::Path;
use std::sync::{Arc, Mutex};
use std::sync::atomic::{AtomicBool};
use std::sync::atomic::Ordering;
use std::thread;
use std::time::{Duration, SystemTime};
use std::u128;

use env_logger::{Builder, Env};
use sha2::{Sha256, Digest};

#[derive(Debug)]
struct BlockchainEntry {
    block: String,
    previous_hash: String,
    data: String,
    file_hash: String,
    nonce: Option<String>
}

impl BlockchainEntry {

    fn deserialize(&self) -> String {
        if self.nonce.is_some() {
            return format!("block={}\nprevious_hash={}\ndata={}\nnonce={}", self.block, self.previous_hash, self.data, self.nonce.clone().unwrap());
        } else {
            return format!("block={}\nprevious_hash={}\ndata={}", self.block, self.previous_hash, self.data);
        }
    }

}

impl Clone for BlockchainEntry {

    fn clone(&self) -> BlockchainEntry {
        BlockchainEntry {
            block: self.block.clone(),
            previous_hash: self.previous_hash.clone(),
            data: self.data.clone(),
            file_hash: self.file_hash.clone(),
            nonce: self.nonce.clone()
        }
    }
}

fn main() {
//    env_logger::init();
    Builder::from_env(Env::default().default_filter_or("info")).init();

    get_cmd_args();
}

fn get_cmd_args() {

    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        error!("Expected path argument");
        return;
    }

    let input_dir = &args[1];

    let mut new_data = None;

    if args.len() == 3 {
        let data = &args[2];
        new_data = Some(data.to_owned());
    }

    match new_data.is_some() {
        true => info!("Finding next nonce for input {} at {}", new_data.clone().unwrap(), input_dir),
        false => info!("Verifying blockchain at {}", input_dir)
    }

    match read_files_from_folder(input_dir, new_data) {
        Ok(_x) => {},
        Err(x) => error!("Validation broke on a file level, error: {}", x)
    }

}


fn read_files_from_folder(path: &String, new_data: Option<String>) -> std::io::Result<()> {

    let mut blockchain_entries: BTreeMap<i32, BlockchainEntry> = BTreeMap::new();


    for entry in fs::read_dir(path)? {
        let dir = entry?;

        let blockchain_entry = read_blockchain_from_file(dir.path().to_str().unwrap());
        debug!("parsed blockchain {:?}", blockchain_entry);

        match blockchain_entry {
            Some(x) => {
                let index: i32 = x.block.parse().expect("Invalid entry number in block");

                if blockchain_entries.contains_key(&index) {
                    error!("Duplicate block {} found, aborting", index);
                    return Ok(());
                }
                blockchain_entries.insert(index, x);

            },
            None => {}
        }


    }

    let is_valid = validate_hashes(&blockchain_entries);

    if new_data.is_none() {
        if is_valid {

            // write validation block entry thing m8
            let (key, value) = blockchain_entries.iter().next_back().unwrap();

            // create new blockchain entry after last element
            let new_entry = BlockchainEntry {
                block: format!("{:02}", key + 1),
                previous_hash: value.file_hash.to_owned(),
                data: String::from("Blockchain richtig."),
                nonce: None,
                file_hash: "".to_string(),
            };

            info!("Blockchain is valid, writing file {}", key + 1);


            let target_dir = Path::new(path);

            let target_path = target_dir.join(&(new_entry.block));

            fs::write(target_path, new_entry.deserialize()).expect("Unable to write file");
        }
    } else {

        let target_dir = Path::new(path);

        if blockchain_entries.len() == 0 {
            // create first entry

            // create new blockchain entry after last element
            let new_entry = BlockchainEntry {
                block: format!("{:02}", 0),
                previous_hash: "null".to_string(),
                data: new_data.unwrap(),
                nonce: None,
                file_hash: "".to_string(),
            };

            let entry_with_nonce = generate_nonce(new_entry);

            info!("first chain entry hash {}", calculate_hash(entry_with_nonce.deserialize()));

            let target_path = target_dir.join(&(entry_with_nonce.block));
            fs::write(target_path, entry_with_nonce.deserialize()).expect("Unable to write file");


        } else {

            // get last element from blockchain entries
            let (key, value) = blockchain_entries.iter().next_back().unwrap();

            // create new blockchain entry after last element
            let new_entry = BlockchainEntry {
                block: format!("{:02}", key + 1),
                previous_hash: value.file_hash.to_owned(),
                data: new_data.unwrap(),
                nonce: None,
                file_hash: "".to_string(),
            };

            let entry_with_nonce = generate_nonce(new_entry);

            info!("Nonce found, next entry would be {:?}", entry_with_nonce);
            info!("entry hash is {}", calculate_hash(entry_with_nonce.deserialize()));

            let target_path = target_dir.join(&(entry_with_nonce.block));
            fs::write(target_path, entry_with_nonce.deserialize()).expect("Unable to write file");


        }
    }

    Ok(())
}

fn validate_hashes(input: &BTreeMap<i32, BlockchainEntry>) -> bool {

    let mut prev_hash = "";
    let mut expected_index: i32 = 0;
    for (k, v) in input.iter() {

        if k.ne(&expected_index) {
            error!("Expected blockchain with number {}, found {}, is your chain complete?", expected_index, k);
            return false;
        }
        expected_index = expected_index + 1;

        debug!("{}: {:?}", k, v);

        if k.ge(&1) {
            debug!("previous calculated hash was {}", prev_hash);
            debug!("previous expected hash {}", v.previous_hash);

            if prev_hash.ne(&v.previous_hash) {
                error!("Hashes did not match up for entry {}! Expected {}, found {}. Aborting validation!", k, prev_hash, v.previous_hash);
                return false;
            }
        } else {
            debug!("first file, next previous_hash should be {}", v.file_hash)
        }

//        println!("next hash should be {:x?}", calculate_hash(format!("{:02}{}", k, &v.data)));
        prev_hash = &v.file_hash;
    }

    true
}

fn calculate_hash(data: String) -> String {
//    println!("calculate_hash: {}", data);

    let mut hasher = Sha256::new();
    hasher.input(data);
    format!("{:x}", hasher.result())
}

fn read_blockchain_from_file(path: &str) -> Option<BlockchainEntry> {

    let file_content = fs::read_to_string(path).expect(format!("Could not open file {} in your directory", path).as_str());


    if !file_content.contains("block=") || !file_content.contains("previous_hash=") || !file_content.contains("data=") {
        return None;
    }

    let mut block = None;
    let mut previous_hash = None;
    let mut data = None;
    let file_hash = calculate_hash(file_content.clone());


    let lines = file_content.split("\n");

    debug!("File content {}", file_content);


    for line in lines {

        if line.trim().contains("block=") {
            let value: Vec<&str> = line.split("=").collect();
            block = Some(value.get(1).expect("block has no value").to_owned().to_string());

        } else if line.trim().contains("previous_hash=") {
            let value: Vec<&str> = line.split("=").collect();
            previous_hash = Some(value.get(1).expect("previous_hash has no value").to_owned().to_string());

        } else if line.trim().contains("data=") {
            let value: Vec<&str> = line.split("=").collect();
            data = Some(value.get(1).expect("block has no value").to_owned().to_string());

        } else if line.trim().contains("nonce=") {
            let value: Vec<&str> = line.split("=").collect();
            data = Some(value.get(1).expect("nonce has no value").to_owned().to_string());

        }

    }

    debug!("read_blockchain_from_file block {:?}", block);
    debug!("read_blockchain_from_file previous_hash {:?}", previous_hash);
    debug!("read_blockchain_from_file data {:?}", data);


    if block.is_none() || previous_hash.is_none() || data.is_none() {
        return None;
    }

    Some(BlockchainEntry {
        block: block.unwrap(),
        previous_hash: previous_hash.unwrap(),
        data: data.unwrap(),
        file_hash,
        nonce: None
    })

}

fn generate_nonce(new_entry: BlockchainEntry) -> BlockchainEntry {

//    let n_threads = 12;
    let n_threads: u128 = num_cpus::get() as u128;

    info!("Generating nonce with {} threads", n_threads);

    let now = SystemTime::now();
    let range: u128 = u128::MAX / n_threads;

//    let has_finished = Arc::new(Mutex::new(AtomicBool::new(false)));
    let has_finished = Arc::new(AtomicBool::new(false));
    let result = Arc::new(Mutex::new(new_entry));


    for i in 0..n_threads {
        let mut result_container = result.clone();
        let mut entry = result.lock().unwrap().clone();
        let mut thread_finished = has_finished.clone();

        thread::spawn(move || {


            let mut counter: u128 = i * range;
            while !thread_finished.load(Ordering::SeqCst) {
                let entry_ref = &mut entry;

                entry_ref.nonce = Some(counter.to_string());

                let new_string = entry_ref.deserialize();

                let new_string_hash = calculate_hash(new_string);

                if is_hash_in_limit(&new_string_hash) {
                    debug!("Thread-{}: Solution found! Nonce is {}", i, counter);
                    // tell others to break
                    thread_finished.store(true, Ordering::SeqCst);
                    let mut result = result_container.lock().unwrap();
                    result.nonce = Some(counter.to_string());

                    break;
                }
                if counter == u128::MAX {
                    error!("Thread-{}: Reached max value", i);
                    break;
                }
                counter = counter + 1;

                if counter % 10000 == 0 {
                    debug!("Thread-{}: at iteration {}", i, counter);
                    debug!("Thread-{}: {:?}", i, entry_ref);
                    debug!("Thread-{}: {}", i, new_string_hash);
                }

//            if counter > 5 {
//                break;
//            }
            }
        });
    }
    while !(has_finished.load(Ordering::SeqCst)) {
        thread::sleep(Duration::from_millis(50));
    }

    let duration = now.elapsed();

    info!("Nonce finding duration: {}s", duration.unwrap().as_secs());

    let x = Arc::try_unwrap(result).expect("Couldn't unwrap Arc");
    x.into_inner().expect("Couldn't unwrap mutex")

}

fn is_hash_in_limit(hash: &str) -> bool {
// holy overflow magic

//    let limit = 16u128.pow(58);
//
//    let hash_value = match u128::from_str_radix(&hash, 16) {
//        Ok(x) => x,
//        Err(_x) => return false
//    };
//
//    limit.gt(&hash_value)

//    println!("{}", hash);
    hash.starts_with("000000")

}