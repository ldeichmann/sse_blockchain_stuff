#intro

This application can be run using one or two command line arguments. The first one will always be a path to a directory,
while the second argument is used to pass data to be written to the blockchain.
- If only one argument is passed, the blockchain in the given directory is validated.
  - If this directory contains a valid blockchain, it will be validated
- If a second argument is passed, this argument will be written into the blockchain with a nonce.
  - If the directory didn't contain a blockchain, a new one will be started using the given data
  - If the directory did contain a blockchain, it will be extended with the data

# How to run

1. Install rust, see https://www.rust-lang.org/learn/get-started (Tested using rust stable 1.32)
  - It might be necessary to install Build Tools for Visual Studio 2017 to build the code
2. Enter this directory
3. Call `cargo run <arg1> <arg2>`, where `<arg1>` is the path to the directory, and `<arg2>` the optional new data

Note: Remember to pass paths and data containing spaces using quotation marks, e.g.

`cargo run "/home/xxx/sse files/ex1/blockchain_ex1/" "All your base are belong to us"`